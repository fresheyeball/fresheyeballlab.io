==================================================================
https://keybase.io/fresheyeball
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://fresheyeball.gitlab.io
  * I am fresheyeball (https://keybase.io/fresheyeball) on keybase.
  * I have a public key ASC6dm8Gdo0tOqRGjGlUztbs_RgdmglaxyM5NpG0zW4H_go

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120fd5f1b1e1b7d8163fe90b4c426b1f7dff5c9878b1a35b29bf5ccb6d5071d7e2b0a",
      "host": "keybase.io",
      "kid": "0120ba766f06768d2d3aa4468c6954ced6ecfd181d9a095ac723393691b4cd6e07fe0a",
      "uid": "0d25a821296d8aa10b2cb6e7e4ae2119",
      "username": "fresheyeball"
    },
    "merkle_root": {
      "ctime": 1606870998,
      "hash": "cae3e29ef0f048de37b5e038ae7ea2958afb50f1474d4324e7f10a094c6f22ddd4060dd4915e4642bc869b6f2866a459b24bb39cd2cd13253fe602c1d67351ee",
      "hash_meta": "cd83fcb701e98c9b0c6266819f95e0837eee0b299c91304f1c4dcce4665e67e5",
      "seqno": 18283850
    },
    "service": {
      "entropy": "BSMxYV4emNyusoH4XFyi4/mb",
      "hostname": "fresheyeball.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.4.2"
  },
  "ctime": 1606871019,
  "expire_in": 504576000,
  "prev": "2c5699df1518405f9931fded483274aa576adc744ae17ee2d11720f606db0fe2",
  "seqno": 81,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgunZvBnaNLTqkRoxpVM7W7P0YHZoJWscjOTaRtM1uB/4Kp3BheWxvYWTESpcCUcQgLFaZ3xUYQF+ZMf3tSDJ0qldq3HRK4X7i0Rcg9gbbD+LEIHzd1ZEYR1Efb6UPjJU75vw4agxvieVVeRwdoPNoVi0mAgHCo3NpZ8RATo5GiLsGNAfQN//oHIrBqdT9Yv0t7cpPzLOCaYSK64C3YU/sVpxT0LkDP49Oum9ETOzH0/nuA4RABISP4gS+DKhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIOMhxD9KcKWCsiXfIbyeI1oHAUtJUOmgA49u+DFkcwtDo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/fresheyeball

==================================================================
